/**
 * Tells CloudCompiler to place this module into an
 * execution unit called myfreshapi (Lambda, K8 Pod, VM, ...)
 * @topology_group myfreshapi
 */

 import { NestFactory } from '@nestjs/core';
 import { AppModule } from './app.module';
 import { ExpressAdapter, NestExpressApplication } from '@nestjs/platform-express';
 import * as express from 'express';
 
 // Standard express object that we bind to Nest
 const localExpressApp = express();
 
 async function bootstrap() {
   
   const app = await NestFactory.create<NestExpressApplication>(
     AppModule,
     new ExpressAdapter(localExpressApp) // Binding happens here
   );  
   app.init();
 
   // If not in the cloud, listen locally
   if (process.env["CLOUDCC"] != "true") { 
     await app.listen(3000); 
   }  
 }
 
 bootstrap();
 
 /**
  * Connects the localExpressApp express object to the Internet
  * @capability https_server
  */
  exports.app = localExpressApp